public class opLogicos {
    public static void main(String[] args) {
        int valor = 30;
        boolean teste = false;
        teste = valor < 40 && valor > 20; //true
        System.out.println(teste);
        teste = valor < 40 && valor > 30; // false
        System.out.println(teste);
        teste = valor > 30 || valor > 20; //true
        System.out.println(teste);
        teste = valor > 30 || valor < 20; // false
        System.out.println(teste);
        teste = valor < 50 && valor == 30; // true
        System.out.println(teste);
    }
    
}
